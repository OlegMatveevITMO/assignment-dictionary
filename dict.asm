extern string_equals

section .text

global find_word

%define key_size 8

find_word:
        test rsi, rsi
        jz .false
        push rdi
        push rsi
        add rsi, key_size
        call string_equals
        pop rsi
        pop rdi
        test rax, rax
        jnz .true
        mov rsi, [rsi]
        jmp find_word
    .false:
        xor rax, rax
        ret
    .true:
        mov rax, rsi
        ret
