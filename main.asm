section .data
	msg: db "No word", 0

global _start

section .text
%include "colon.inc"
%include "words.inc"

%define BUFFER_SIZE 256
%define key_size 8
%define SUCCESS 0
%define ERROR_STO 1
%define ERROR_STE 2


extern read_word
extern find_word
extern string_length
extern print_string
extern print_newline
extern exit


_start:
	mov rsi, BUFFER_SIZE
	sub rsp, BUFFER_SIZE

	mov rdi, rsp
	call read_word

	test rax, rax
	je .false

	mov rdi, rax
	mov rsi, next
	call find_word
	test rax, rax
	je .false

	add rax, key_size
    push rax
    mov rax, [rsp]
    mov rdi, rax
	call string_length
    pop rdi
    add rdi, rax
    inc rdi
	mov r13, ERROR_STO
	call print_string
	mov rdi, SUCCESS
	call exit
.false:
	mov rdi, msg
	call string_length
	mov rsi, rax
	mov r13, ERROR_STE
	call print_string
	mov rdi, ERROR_STO
	call exit
