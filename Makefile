ASM=nasm
ASMFLAGS=-f elf64
LD=ld

all: start

main.o: lib.inc colon.inc words.inc main.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
	
start: lib.o dict.o main.o
	$(LD) -o $@ $^

clean:
	rm -f *.o start

.PHONY: clean
