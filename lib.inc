section .text
global string_length
global print_char
global print_newline
global print_string
global print_error
global print_uint
global print_int
global string_equals
global parse_uint
global parse_int
global read_word
global string_copy
global exit



; Принимает код возврата и завершает текущий процесс
exit:
    mov     rax, 60
    xor     rdi, rdi
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    xor rax, rax
.count:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .count
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    mov rsi, rdi
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rax
    push rdi
    push rsi
    push rdx
    dec rsp
    mov byte [rsp], dil
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    inc rsp
    pop rdx
    pop rsi
    pop rdi
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rax, 1
    mov rdi, 1
    mov rsi, 0xA
    mov rdx, 1
    syscall
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    push r12
    push r13
    mov r12, rsp
    mov r13, 10
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0
.loop:
    dec rsp
    xor rdx, rdx
    div  r13
    add rdx, 0x30
    mov  byte[rsp], dl
    test rax, rax
    jz .print
    jmp .loop
.print:
    mov rdi, rsp
    call print_string
    mov rsp, r12
    pop r13
    pop r12
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    push rdi
    cmp rdi, 0
    jge .print
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.print:
    call print_uint
    pop rdi
    ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    push rbx
.loop:
    mov bl, byte [rsi + rax]
    cmp byte [rdi + rax], bl
    jne .err
    cmp byte [rdi + rax], 0
    je .equals
    inc rax
    jmp .loop
.err:
    xor rax, rax
    jmp .end
.equals:
    mov rax, 1
.end:
    pop rbx
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rdi
    push rsi
    push rdx
    dec rsp
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    je .str_end
    xor rax, rax
    mov al, byte [rsp]
    jmp .end
.str_end:
    xor rax, rax
.end:
    inc rsp
    pop rdx
    pop rsi
    pop rdi
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
.whitespaces:
    call read_char
    cmp rax, ' '
    je .whitespaces
    cmp rax, 0x9
    je .whitespaces
    cmp rax, 0xA
    je .whitespaces
    xor rdx, rdx
.loop:
    cmp rdx, rsi
    je .fail
    cmp rax, ' '
    je .succ
    cmp rax, 0x9
    je .succ
    cmp rax, 0xA
    je .succ
    test rax, rax
    je .succ
    mov byte [rdi + rdx], al
    inc rdx
    call read_char
    jmp .loop
.fail:
    xor rax, rax
    jmp .end
.succ:
    mov rax, rdi
    mov byte [rdi + rdx], 0
.end:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    xor rcx, rcx
    xor rdx, rdx
    mov r10, 10
.loop:
    mov sil, [rdi+rcx]
    cmp sil, 0x30
    jl .return
    cmp sil, 0x39
    jg .return
    inc rcx
    sub sil, 0x30
    mul r10
    add rax, rsi
    jmp .loop
.return:
    mov rdx, rcx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
xor rax, rax
    cmp byte [rdi], 0x2d
    je parse
    call parse_uint
    ret
parse:
    inc rdi
    call parse_uint
    cmp rdx, 0
    je .return
    neg rax
    inc rdx
.return:
    ret




; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rbx
    xor rax, rax
.loop:
    cmp rax, rdx
    je .err
    mov bl, byte [rdi + rax]
    mov byte [rsi + rax], bl
    cmp byte [rsi + rax], 0
    je .end
    inc rax
    jmp .loop
.err:
    xor rax, rax
.end:
    pop rbx
    ret



